# vuepress-plugin-quasar-copy

> A vuepress plugin for clipboard-copy based on Quasar.

[NPM](https://www.npmjs.com/package/vuepress-plugin-quasar-copy)
[Bitbucket](https://bitbucket.org/I_Eki/vuepress-plugin-quasar-copy)
[LICENSE](https://bitbucket.org/I_Eki/vuepress-plugin-quasar-copy/src/master/LICENSE)

## Install

```bash
# install dependencies
npm i vuepress-plugin-quasar-copy -D

# or use yarn
yarn add vuepress-plugin-quasar-copy -D
```

## Usage

Write vuepress config

```javascript
module.exports = {
  plugins: ["quasar-copy"],
};
```

Write Quasar config

```javascript
// docs/.vuepress/clientAppEnhance.js

import { Quasar, Notify } from "quasar";

// Import Quasar css
import "quasar/src/css/index.sass";

export default defineClientAppEnhance(({ app }) => {
  app.use(
    Quasar,
    {
      plugins: {
        Notify,
      },
      // config: {
      //   notify: {
      //     /* look at QuasarConfOptions from the API card on https://quasar.dev/quasar-plugins/notify#notify-api*/
      //   },
      // },
    },
    {}
  );
});
```

## Options

This plugin supports the following configurations.

```javascript
module.exports = {
  plugins: [
    "one-click-copy",
    {
      copySelector: [
        'div[class*="language-"] pre',
        'div[class*="aside-code"] aside',
      ], // String or Array
      copyMessage: "Copy successfully!", // default is 'Copy successfully!'
      FAILED_MESSAGE: "Copy failed!", // default is 'Copy failed!'
      duration: 3000, // prompt message display time.
      showInMobile: false, // whether to display on the mobile side, default: false.
    },
  ],
};
```

## License

[MIT](https://bitbucket.org/I_Eki/vuepress-plugin-quasar-copy/src/master/LICENSE).
