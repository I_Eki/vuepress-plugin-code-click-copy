import './assets/css/style.css';
import { onMounted } from 'vue';

export function copyToClipboard(str) {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false;
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
}

export default () => {
  let scrollEventAdded = false;
  const isMobile =
    !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent,
    );

  const initCopy = (mutations, observer) => {
    for (const mutation of mutations) {
      if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
        if (typeof __GLOBAL_COPY_SELECTOR === 'string') {
          document.querySelectorAll(__GLOBAL_COPY_SELECTOR).forEach(generateCopyButton);
        } else if (
          __GLOBAL_COPY_SELECTOR instanceof Array ||
          Array.isArray(__GLOBAL_COPY_SELECTOR)
        ) {
          __GLOBAL_COPY_SELECTOR.forEach((item) => {
            document.querySelectorAll(item).forEach((p) => {
              generateCopyButton(p);
            });
          });
        }
      }
    }
  };

  const generateCopyButton = (parent) => {
    if (parent.classList.contains('codecopy-enabled')) return;

    const copyBubble = document.createElement('i');
    copyBubble.className = 'code-copy-bubble hide';

    const copyElement = document.createElement('i');
    copyElement.className = 'code-copy';
    copyElement.innerHTML =
      '<svg  style="color:#aaa;font-size:14px" t="1572422231464" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3201" width="20" height="20"><path d="M866.461538 39.384615H354.461538c-43.323077 0-78.769231 35.446154-78.76923 78.769231v39.384616h472.615384c43.323077 0 78.769231 35.446154 78.769231 78.76923v551.384616h39.384615c43.323077 0 78.769231-35.446154 78.769231-78.769231V118.153846c0-43.323077-35.446154-78.769231-78.769231-78.769231z m-118.153846 275.692308c0-43.323077-35.446154-78.769231-78.76923-78.769231H157.538462c-43.323077 0-78.769231 35.446154-78.769231 78.769231v590.769231c0 43.323077 35.446154 78.769231 78.769231 78.769231h512c43.323077 0 78.769231-35.446154 78.76923-78.769231V315.076923z m-354.461538 137.846154c0 11.815385-7.876923 19.692308-19.692308 19.692308h-157.538461c-11.815385 0-19.692308-7.876923-19.692308-19.692308v-39.384615c0-11.815385 7.876923-19.692308 19.692308-19.692308h157.538461c11.815385 0 19.692308 7.876923 19.692308 19.692308v39.384615z m157.538461 315.076923c0 11.815385-7.876923 19.692308-19.692307 19.692308H216.615385c-11.815385 0-19.692308-7.876923-19.692308-19.692308v-39.384615c0-11.815385 7.876923-19.692308 19.692308-19.692308h315.076923c11.815385 0 19.692308 7.876923 19.692307 19.692308v39.384615z m78.769231-157.538462c0 11.815385-7.876923 19.692308-19.692308 19.692308H216.615385c-11.815385 0-19.692308-7.876923-19.692308-19.692308v-39.384615c0-11.815385 7.876923-19.692308 19.692308-19.692308h393.846153c11.815385 0 19.692308 7.876923 19.692308 19.692308v39.384615z" p-id="3202"></path></svg>';
    copyElement.title = 'Click to Copy to Clipboard';
    copyElement.addEventListener('click', () => {
      try {
        copyToClipboard(parent.innerText);
        copyBubble.innerText = __GLOBAL_COPY_MESSAGE;
      } catch (error) {
        copyBubble.innerText = __GLOBAL_FAILED_MESSAGE;
      } finally {
        copyBubble.classList.toggle('hide');
      }
    });
    copyElement.addEventListener('mouseleave', () => {
      copyBubble.classList.add('hide');
    });

    copyElement.appendChild(copyBubble);

    parent.appendChild(copyElement);
    parent.classList.add('codecopy-enabled');
  };

  onMounted(() => {
    if (!isMobile || __GLOBAL_SHOW_IN_MOBILE) {
      const observer = new MutationObserver(initCopy);
      const obConfig = {
        childList: true,
      };

      const app = document.getElementById('app');
      observer.observe(app, obConfig);
    }
  });
};
